package com.junioroffers.infrastructure;

import java.util.List;

import com.junioroffers.infrastructure.offer.dto.JobOfferDto;

public interface RemoteOfferClient {

    List<JobOfferDto> getOffers();
}
