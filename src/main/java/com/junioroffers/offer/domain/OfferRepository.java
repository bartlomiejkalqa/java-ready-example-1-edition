package com.junioroffers.offer.domain;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface OfferRepository extends MongoRepository<Offer, String> {

    boolean existsByOfferUrl(String offerUrl);
    Optional<Offer> findByOfferUrl(String offerUrl);
}
