package com.junioroffers.offer.domain.exceptions;

import lombok.Getter;

@Getter
public class OfferDuplicateException extends RuntimeException {

    private static final long serialVersionUID = -6378244667204645097L;
    private final String offerUrl;

    public OfferDuplicateException(String offerUrl) {
        super(String.format("Offer with offerUrl [%s] already exists", offerUrl));
        this.offerUrl = offerUrl;
    }
}
