package com.junioroffers.offer.domain.exceptions;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class OfferPostErrorResponse {

    private final List<String> messages;
    private final HttpStatus status;
}
