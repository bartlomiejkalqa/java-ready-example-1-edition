package com.junioroffers.offer.domain;

import com.junioroffers.infrastructure.offer.dto.JobOfferDto;
import com.junioroffers.offer.domain.dto.OfferDto;

public class OfferMapper {

    public static OfferDto mapToOfferDto(Offer offer) {
        return OfferDto.builder()
                .id(offer.getId())
                .companyName(offer.getCompanyName())
                .position(offer.getPosition())
                .salary(offer.getSalary())
                .offerUrl(offer.getOfferUrl())
                .build();
    }

    public static Offer mapFromJobOfferDto(JobOfferDto jobOfferDto) {
        final Offer offer = new Offer();
        offer.setSalary(jobOfferDto.getSalary());
        offer.setOfferUrl(jobOfferDto.getOfferUrl());
        offer.setPosition(jobOfferDto.getTitle());
        offer.setCompanyName(jobOfferDto.getCompany());
        return offer;
    }

    public static Offer mapFromOfferDtoToOffer(OfferDto offerDto) {
        final Offer offer = new Offer();
        offer.setSalary(offerDto.getSalary());
        offer.setOfferUrl(offerDto.getOfferUrl());
        offer.setPosition(offerDto.getPosition());
        offer.setCompanyName(offerDto.getCompanyName());
        return offer;
    }
}
