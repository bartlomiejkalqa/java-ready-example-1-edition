package com.junioroffers.offer.domain;

import java.util.List;
import java.util.stream.Collectors;

import com.junioroffers.infrastructure.offer.dto.JobOfferDto;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.exceptions.OfferDuplicateException;
import com.junioroffers.offer.domain.exceptions.OfferNotFoundException;
import org.springframework.dao.DuplicateKeyException;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OfferService {

    private final OfferRepository offerRepository;

    @Cacheable(cacheNames = "jobOffers")
    public List<OfferDto> findAllOffers() {
        return offerRepository.findAll()
                .stream()
                .map(OfferMapper::mapToOfferDto)
                .collect(Collectors.toList());
    }

    public OfferDto findOfferById(String id) {
        return offerRepository.findById(id)
                .map(OfferMapper::mapToOfferDto)
                .orElseThrow(() -> new OfferNotFoundException(id));
    }

    public List<Offer> saveAll(List<Offer> offers) {
        return offerRepository.saveAll(offers);
    }

    public List<Offer> saveAllJobOfferDto(List<JobOfferDto> jobOffers) {
        final List<Offer> offers = mapOffersThatDoesntExistsInDatabase(jobOffers);
        return offerRepository.saveAll(offers);
    }

    private List<Offer> mapOffersThatDoesntExistsInDatabase(List<JobOfferDto> jobOffers) {
        return jobOffers.stream()
                .filter(jobOfferDto -> !jobOfferDto.getOfferUrl().isEmpty())
                .filter(jobOfferDto -> !offerRepository.existsByOfferUrl(jobOfferDto.getOfferUrl()))
                .map(OfferMapper::mapFromJobOfferDto)
                .collect(Collectors.toList());
    }

    public OfferDto saveOffer(OfferDto offerDto) {
        final Offer offer = OfferMapper.mapFromOfferDtoToOffer(offerDto);
        try {
            final Offer save = offerRepository.save(offer);
            return OfferMapper.mapToOfferDto(save);
        } catch (DuplicateKeyException e) {
            throw new OfferDuplicateException(offerDto.getOfferUrl());
        }
    }
}
