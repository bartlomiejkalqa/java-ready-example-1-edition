package com.junioroffers.offer.domain.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Builder
@Getter
@EqualsAndHashCode
public class OfferDto implements Serializable {

    private static final long serialVersionUID = 5496434161301978729L;

    private final String id;

    @NotNull(message = "{companyName.not.null}")
    @NotEmpty(message = "{companyName.not.empty}")
    private final String companyName;

    @NotNull(message = "{position.not.null}")
    @NotEmpty(message = "{position.not.empty}")
    private final String position;

    @NotNull(message = "{salary.not.null}")
    @NotEmpty(message = "{salary.not.empty}")
    private final String salary;

    @NotNull(message = "{offerUrl.not.null}")
    @NotEmpty(message = "{offerUrl.not.empty}")
    private final String offerUrl;
}
