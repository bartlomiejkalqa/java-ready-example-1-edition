package com.junioroffers.offer.scheduling;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.junioroffers.infrastructure.RemoteOfferClient;
import com.junioroffers.infrastructure.offer.dto.JobOfferDto;
import com.junioroffers.offer.domain.Offer;
import com.junioroffers.offer.domain.OfferService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class HttpOffersScheduler {

    private static final Logger log = LoggerFactory.getLogger(HttpOffersScheduler.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    private static final String STARTED_OFFERS_FETCHING_MESSAGE = "Started offers fetching {}";
    private static final String STOPPED_OFFERS_FETCHING_MESSAGE = "Stopped offers fetching {}";
    private static final String ADDED_NEW_OFFERS_MESSAGE = "Added new {} offers";

    private final RemoteOfferClient offerHttpClient;
    private final OfferService offerService;

    @Scheduled(fixedDelayString = "${http.offers.scheduler.request.delay}")
    public void getOffersFromHttpService() {
        log.info(STARTED_OFFERS_FETCHING_MESSAGE, dateFormat.format(new Date()));
        final List<JobOfferDto> offers = offerHttpClient.getOffers();
        final List<Offer> addedOffers = offerService.saveAllJobOfferDto(offers);
        log.info(ADDED_NEW_OFFERS_MESSAGE, addedOffers.size());
        log.info(STOPPED_OFFERS_FETCHING_MESSAGE, dateFormat.format(new Date()));
    }
}
