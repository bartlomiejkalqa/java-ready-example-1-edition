package com.junioroffers.infrastructure.offer.dto;

public interface SampleJobOfferDto {

    default JobOfferDto sampleJobOfferDto(String offerUrl, String company, String title, String salary){
        return JobOfferDto.builder()
                .offerUrl(offerUrl)
                .company(company)
                .title(title)
                .salary(salary)
                .build();
    }

}