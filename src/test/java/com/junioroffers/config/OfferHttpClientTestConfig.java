package com.junioroffers.config;

import com.junioroffers.infrastructure.RemoteOfferClient;
import org.springframework.web.client.RestTemplate;

public class OfferHttpClientTestConfig extends OfferHttpClientConfig {

    public RemoteOfferClient remoteOfferTestClient(String uri, int port, int connectionTimeout, int readTimeout) {
        final RestTemplate restTemplate = restTemplate(connectionTimeout, readTimeout, restTemplateResponseErrorHandler());
        return remoteOfferClient(restTemplate, uri, port);
    }
}
