package com.junioroffers.mongo.config;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.junioroffers.security.login.domain.User;
import com.junioroffers.security.login.domain.UserRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;

@ChangeLog(order = "2")
@Profile("container")
public class DatabaseChangeLogTest {

    @ChangeSet(order = "002", id = "addExistentUser", author = "bartlomiej.kalka")
    @Profile("container")
    public void addUserToCollection(UserRepository userRepository, PasswordEncoder bCryptPasswordEncoder) {
        final User entity = new User();
        entity.setUsername("ExistentUser");
        entity.setPassword(bCryptPasswordEncoder.encode("password"));
        userRepository.insert(entity);
    }
}