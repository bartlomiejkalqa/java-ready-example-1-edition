package com.junioroffers.security.login;

import com.junioroffers.security.login.domain.dto.LoginRequestDto;

public interface SampleLoginRequestDto {

    default LoginRequestDto sampleLoginRequestDto(String userName, String password) {
        return new LoginRequestDto(userName, password);
    }
}
