package com.junioroffers.security.login;

import java.util.Collections;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.junioroffers.JobOffersApplication;
import com.junioroffers.exceptions.ApiValidationErrorDto;
import com.junioroffers.security.login.domain.UserRepository;
import com.junioroffers.security.login.domain.dto.JwtResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = LoginControllerIntegrationTest.TestConfig.class)
@AutoConfigureMockMvc
@ActiveProfiles({"container"})
@Testcontainers
class LoginControllerIntegrationTest {

    private static final String MONGO_VERSION = "4.4.4";

    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo:" + MONGO_VERSION);

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    public void should_return_200_success_with_jwt_token_when_user_is_valid(@Autowired MockMvc mockMvc,
                                                                            @Autowired UserRepository userRepository,
                                                                            @Autowired ObjectMapper objectMapper) throws Exception {
        assertThat(userRepository.findByUsername("ExistentUser")).isNotEmpty();
        String body = getExistentUser();

        MvcResult result = mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(status().isOk())
                .andReturn();

        final String contentAsString = result.getResponse().getContentAsString();
        final JwtResponse jwtResponse = objectMapper.readValue(contentAsString, JwtResponse.class);
        assertThat(jwtResponse.getUsername()).isEqualTo("ExistentUser");
        assertThat(jwtResponse.getToken()).isNotBlank();
    }

    @Test
    public void should_return_401_unauthorized_when_username_or_password_is_not_valid(@Autowired MockMvc mockMvc,
                                                                                      @Autowired UserRepository userRepository) throws Exception {
        assertThat(userRepository.findByUsername("NotExistentUser")).isEmpty();
        String body = getNotExistentUser();

        mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }

    @Test
    public void should_return_400_bad_request_when_request_body_is_not_valid_no_username(@Autowired MockMvc mockMvc,
                                                                                         @Autowired ObjectMapper objectMapper) throws Exception {
        ApiValidationErrorDto expectedResponse = new ApiValidationErrorDto(Collections.singletonList(
                "userName must not be empty and must not be null"), HttpStatus.BAD_REQUEST);
        String body = "{\"password\":\"passwd\"}";

        final MvcResult mvcResult = mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(status().isBadRequest())
                .andReturn();
        final String contentAsString = mvcResult.getResponse().getContentAsString();
        final ApiValidationErrorDto apiValidationErrorDto = objectMapper.readValue(contentAsString, ApiValidationErrorDto.class);

        assertThat(apiValidationErrorDto).isEqualTo(expectedResponse);
    }

    @Test
    public void should_return_400_bad_request_when_request_body_is_not_valid_no_password(@Autowired MockMvc mockMvc,
                                                                                         @Autowired ObjectMapper objectMapper) throws Exception {
        ApiValidationErrorDto expectedResponse = new ApiValidationErrorDto(Collections.singletonList(
                "password must not be empty and must not be null"), HttpStatus.BAD_REQUEST);
        String body = "{\"userName\":\"user\"}";

        final MvcResult mvcResult = mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(status().isBadRequest())
                .andReturn();

        final String contentAsString = mvcResult.getResponse().getContentAsString();
        final ApiValidationErrorDto apiValidationErrorDto = objectMapper.readValue(contentAsString, ApiValidationErrorDto.class);

        assertThat(apiValidationErrorDto).isEqualTo(expectedResponse);
    }

    private String getExistentUser() {
        return getUser("ExistentUser", "password");
    }

    private String getNotExistentUser() {
        return getUser("NotExistentUser", "password");
    }

    private String getUser(String userName, String password) {
        return "{\"userName\":\"" + userName + "\", \"password\":\"" + password + "\"}";
    }

    @Import(JobOffersApplication.class)
    static class TestConfig {

    }
}