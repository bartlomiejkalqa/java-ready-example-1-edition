package com.junioroffers.security.login;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.junioroffers.JobOffersApplication;
import com.junioroffers.security.login.domain.UserRepository;
import com.junioroffers.security.login.domain.dto.JwtResponse;
import com.junioroffers.security.login.domain.exceptions.LoginErrorResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = LoginAndOffersPathControllersIntegrationTest.TestConfig.class)
@AutoConfigureMockMvc
@ActiveProfiles({"container"})
@Testcontainers
class LoginAndOffersPathControllersIntegrationTest {

    private static final String MONGO_VERSION = "4.4.4";

    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo:" + MONGO_VERSION);

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    public void should_return_offers_response_200_success_after_positive_authentication_with_jwt_token(
            @Autowired MockMvc mockMvc,
            @Autowired UserRepository userRepository,
            @Autowired ObjectMapper objectMapper) throws Exception {

        assertThat(userRepository.findByUsername("ExistentUser")).isNotEmpty();

        String body = getExistentUser();

        MvcResult result = mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(status().isOk())
                .andReturn();

        final String content = result.getResponse().getContentAsString();
        final JwtResponse jwtResponse = objectMapper.readValue(content, JwtResponse.class);
        String token = jwtResponse.getToken();

        mockMvc.perform(get("/offers")
                .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void should_return_offers_response_401_unauthorized_after_negative_authentication_with_jwt_token(
            @Autowired MockMvc mockMvc,
            @Autowired UserRepository userRepository,
            @Autowired ObjectMapper objectMapper) throws Exception {

        final LoginErrorResponse expectedLoginErrorResponse = new LoginErrorResponse(
                "Bad Credentials",
                HttpStatus.UNAUTHORIZED
        );

        assertThat(userRepository.findByUsername("NotExistentUser")).isEmpty();

        String body = getNotExistentUser();

        MvcResult result = mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(status().isUnauthorized())
                .andReturn();

        final String content = result.getResponse().getContentAsString();
        final LoginErrorResponse loginErrorResponse = objectMapper.readValue(content, LoginErrorResponse.class);

        assertThat(loginErrorResponse).isEqualTo(expectedLoginErrorResponse);

        mockMvc.perform(get("/offers"))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }

    private String getExistentUser() {
        return getUser("ExistentUser", "password");
    }

    private String getNotExistentUser() {
        return getUser("NotExistentUser", "password");
    }

    private String getUser(String userName, String password) {
        return "{\"userName\":\"" + userName + "\", \"password\":\"" + password + "\"}";
    }

    @Import(JobOffersApplication.class)
    static class TestConfig {

    }
}