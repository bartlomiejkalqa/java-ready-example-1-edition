package com.junioroffers.offer;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.junioroffers.config.MessageSourceConfig;
import com.junioroffers.exceptions.ApiValidationErrorHandler;
import com.junioroffers.offer.domain.OfferRepository;
import com.junioroffers.offer.domain.OfferService;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.exceptions.OfferControllerErrorHandler;
import com.junioroffers.offer.domain.exceptions.OfferErrorResponse;
import com.junioroffers.offer.domain.exceptions.SampleOfferNotFoundException;
import com.junioroffers.security.SecurityConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@ContextConfiguration(classes = MockMvcConfig.class)
class OfferControllerTest implements SampleOfferDto {

    @Test
    public void should_return_status_ok_when_get_for_offers(@Autowired MockMvc mockMvc, @Autowired ObjectMapper objectMapper) throws Exception {
        final List<OfferDto> expectedOfferDtos = Arrays.asList(cybersourceOfferDto(), cdqPolandOfferDto());
        String expectedResponseBody = objectMapper.writeValueAsString(expectedOfferDtos);

        final MvcResult mvcResult = mockMvc.perform(get("/offers"))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    public void should_return_status_ok_when_get_for_offer_with_id(@Autowired MockMvc mockMvc, @Autowired ObjectMapper objectMapper) throws Exception {
        String expectedResponseBody = objectMapper.writeValueAsString(cybersourceOfferDto());

        final MvcResult mvcResult = mockMvc.perform(get("/offers/7b3e02b3-6b1a-4e75-bdad-cef5b279b074"))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    public void should_return_status_not_found_when_get_for_offer_with_not_found_id(@Autowired MockMvc mockMvc, @Autowired ObjectMapper objectMapper) throws Exception {
        OfferErrorResponse offerErrorResponse = new OfferErrorResponse("Offer with id 555 not found", HttpStatus.NOT_FOUND);
        String expectedResponseBody = objectMapper.writeValueAsString(offerErrorResponse);

        final MvcResult mvcResult = mockMvc.perform(get("/offers/555"))
                .andExpect(status().isNotFound())
                .andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    public void should_return_status_ok_and_expected_offer_resource(@Autowired MockMvc mockMvc, @Autowired ObjectMapper objectMapper) throws Exception {
        final String cdqPolandOfferDto = objectMapper.writeValueAsString(cdqPolandOfferDto());

        final MvcResult mvcResult = mockMvc.perform(
                post("/offers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(cdqPolandOfferDto))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(cdqPolandOfferDto);
    }

    @Test
    public void should_return_status_not_found_and_empty_response(@Autowired MockMvc mockMvc) throws Exception {
        final MvcResult mvcResult = mockMvc.perform(
                post("/offers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(wrongOfferPostJson()))
                .andExpect(status().isBadRequest())
                .andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(expectedErrorMessages());
    }

    private List<String> expectedErrorMessages() {
        return Arrays.asList(
                "offerUrl must not be null",
                "offerUrl must not be empty",
                "position must not be empty",
                "position must not be null",
                "salary must not be empty",
                "salary must not be null");
    }

    private String wrongOfferPostJson() {
        return "{\"companyName\":\"test\"}";
    }
}

@Import({SecurityConfig.class, JwtTestConfig.class, MessageSourceConfig.class})
class MockMvcConfig implements SampleOfferDto, SampleOfferNotFoundException {

    @Bean
    ApiValidationErrorHandler apiValidationErrorResponseHandler(){
        return new ApiValidationErrorHandler();
    }

    @Bean
    OfferControllerErrorHandler offerControllerErrorHandler() {
        return new OfferControllerErrorHandler();
    }

    @Bean
    OfferService offerService() {
        OfferRepository offerRepository = mock(OfferRepository.class);
        return new OfferService(offerRepository) {
            @Override
            public List<OfferDto> findAllOffers() {
                return Arrays.asList(cybersourceOfferDto(), cdqPolandOfferDto());
            }

            @Override
            public OfferDto findOfferById(String id) {
                if ("7b3e02b3-6b1a-4e75-bdad-cef5b279b074".equals(id)) {
                    return cybersourceOfferDto();
                } else if ("24ee32b6-6b15-11eb-9439-0242ac130002".equals(id)) {
                    return cdqPolandOfferDto();
                }
                throw sampleOfferNotFoundException(id);
            }

            @Override
            public OfferDto saveOffer(OfferDto offerDto) {
                return cdqPolandOfferDto();
            }
        };
    }

    @Bean
    OfferController offerController(OfferService offerService) {
        return new OfferController(offerService);
    }
}