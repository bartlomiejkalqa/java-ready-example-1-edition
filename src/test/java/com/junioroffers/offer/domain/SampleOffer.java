package com.junioroffers.offer.domain;

public interface SampleOffer {

    default Offer sampleOffer(String id, String companyName, String position, String salary, String offerUrl) {
        return new Offer(id, companyName, position, salary, offerUrl);
    }

    default Offer sampleOfferCdqPoland() {
        return new Offer("24ee32b6-6b15-11eb-9439-0242ac130002",
                "CDQ Poland",
                "Junior DevOps Engineer",
                "8k - 14k PLN",
                "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd");
    }

    default Offer sampleOfferWithoutId(String companyName, String position, String salary, String offerUrl) {
        final Offer offer = new Offer();
        offer.setCompanyName(companyName);
        offer.setOfferUrl(offerUrl);
        offer.setPosition(position);
        offer.setSalary(salary);
        return offer;
    }
}