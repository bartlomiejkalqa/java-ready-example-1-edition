package com.junioroffers.offer.domain;

import java.util.Arrays;
import java.util.List;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.exceptions.OfferNotFoundException;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

@SpringBootTest(classes = JobOffersApplication.class)
@ActiveProfiles("container")
@Testcontainers
class OfferServiceWithContainerTests implements SampleOffer, SampleOfferDto {

    private static final String MONGO_VERSION = "4.4.4";

    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo:" + MONGO_VERSION);

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    void should_return_all_offers(@Autowired OfferRepository repository,
                                  @Autowired OfferService service) {

        Offer cybersourceOffer = sampleOffer("7b3e02b3-6b1a-4e75-bdad-cef5b279b074", "Cybersource", "Software Engineer - Mobile (m/f/d)", "4k - 8k PLN", "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn");
        Offer cdq = sampleOffer("24ee32b6-6b15-11eb-9439-0242ac130002", "CDQ Poland", "Junior DevOps Engineer", "8k - 14k PLN", "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd");
        then(repository.findAll()).containsAll(Arrays.asList(cybersourceOffer, cdq));

        final List<OfferDto> allOffers = service.findAllOffers();

        assertThat(allOffers).containsAll(Arrays.asList(cybersourceOfferDto(), cdqPolandOfferDto()));
    }

    @Test
    void should_return_offer_by_id(@Autowired OfferRepository repository,
                                   @Autowired OfferService service) {

        OfferDto cybersourceOfferDto = cybersourceOfferDto();
        then(repository.findById("7b3e02b3-6b1a-4e75-bdad-cef5b279b074")).isPresent();

        final OfferDto offerById = service.findOfferById("7b3e02b3-6b1a-4e75-bdad-cef5b279b074");

        assertThat(offerById).isEqualTo(cybersourceOfferDto);
    }

    @Test
    void should_throw_not_found_exception(@Autowired OfferRepository repository,
                                          @Autowired OfferService service) {
        then(repository.findById("aaa")).isNotPresent();

        Throwable thrown = catchThrowable(() -> service.findOfferById("aaa"));

        AssertionsForClassTypes.assertThat(thrown)
                .isInstanceOf(OfferNotFoundException.class)
                .hasMessage("Offer with id aaa not found");
    }
}
