package com.junioroffers.offer.domain;

import java.util.Arrays;
import java.util.List;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;

@SpringBootTest(classes = OfferServiceSaveAllWithContainerTests.TestConfig.class)
@ActiveProfiles("container")
@Testcontainers
class OfferServiceSaveAllWithContainerTests implements SampleOffer, SampleOfferDto {

    private static final String MONGO_VERSION = "4.4.4";

    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo:" + MONGO_VERSION);

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    void should_add_two_offers_using_service(@Autowired OfferRepository repository,
                               @Autowired OfferService service) {
        final Offer sampleOffer = sampleOfferWithoutId("companynamefirstTest",
                "urlfirstTest",
                "positionfirstTest",
                "40kfirstTest");
        final Offer sampleOffer2 = sampleOfferWithoutId("companynamesecondTest",
                "urlsecondTest",
                "positionsecondTest",
                "40ksecondTest");
        final List<Offer> offers = Arrays.asList(sampleOffer, sampleOffer2);
        then(repository.findAll()).doesNotContainAnyElementsOf(offers);

        final List<Offer> savedOffers = service.saveAll(offers);

        assertThat(repository.findAll()).containsAnyElementsOf(offers);
        assertThat(savedOffers).containsAnyElementsOf(offers);
    }

    @Import(JobOffersApplication.class)
    static class TestConfig {

    }
}
