package com.junioroffers.offer.domain;

import java.util.Arrays;
import java.util.List;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.infrastructure.offer.dto.JobOfferDto;
import com.junioroffers.infrastructure.offer.dto.SampleJobOfferDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;

@SpringBootTest(classes = OfferServiceSaveAllWithAlreadyExistingOfferWithContainerTests.TestConfig.class)
@ActiveProfiles({"container", "test"})
@Testcontainers
class OfferServiceSaveAllWithAlreadyExistingOfferWithContainerTests implements SampleOffer, SampleJobOfferDto {

    private static final String MONGO_VERSION = "4.4.4";

    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo:" + MONGO_VERSION);

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    void should_add_only_one_offer_if_offer_with_url_already_exists(@Autowired OfferRepository repository,
                                                                    @Autowired OfferService service) {
        final JobOfferDto shouldAddJobOffer = sampleJobOfferDto("UNIQUE",
                "urlfirstTest",
                "positionfirstTest",
                "40kfirstTest");
        final JobOfferDto shouldNotAddJobOffer = sampleJobOfferDto("NOT_UNIQUE",
                "urlsecondTest",
                "positionsecondTest",
                "40ksecondTest");
        final Offer existingOffer = sampleOfferWithoutId("companynamefirstTest",
                "urlfirstTest",
                "positionfirstTest",
                "NOT_UNIQUE");

        repository.save(existingOffer);
        then(repository.existsByOfferUrl("NOT_UNIQUE")).isTrue();

        final List<Offer> savedOffers = service.saveAllJobOfferDto(Arrays.asList(shouldAddJobOffer, shouldNotAddJobOffer));

        assertThat(savedOffers.size()).isEqualTo(1);
        assertThat(repository.findAll().size()).isEqualTo(2);
        assertThat(repository.existsByOfferUrl("UNIQUE")).isTrue();
    }

    @Import(JobOffersApplication.class)
    static class TestConfig {

    }
}
