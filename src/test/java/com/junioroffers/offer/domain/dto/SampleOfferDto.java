package com.junioroffers.offer.domain.dto;

import com.junioroffers.offer.domain.Offer;
import com.junioroffers.offer.domain.OfferMapper;
import com.junioroffers.offer.domain.SampleOffer;

public interface SampleOfferDto extends SampleOffer {

    default OfferDto sampleOfferDto(String id, String companyName, String position, String salary, String offerUrl) {
        final Offer offer = sampleOffer(id, companyName, position, salary, offerUrl);
        return OfferMapper.mapToOfferDto(offer);
    }

    default OfferDto sampleOfferDtoWithoutId(String companyName, String position, String salary, String offerUrl) {
        final Offer offer = sampleOfferWithoutId(companyName, position, salary, offerUrl);
        return OfferMapper.mapToOfferDto(offer);
    }

    default OfferDto cdqPolandOfferDto() {
        final String id = "24ee32b6-6b15-11eb-9439-0242ac130002";
        final String companyName = "CDQ Poland";
        final String position = "Junior DevOps Engineer";
        final String salary = "8k - 14k PLN";
        final String offerUrl = "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd";
        return sampleOfferDto(id, companyName, position, salary, offerUrl);
    }

    default OfferDto cybersourceOfferDto() {
        final String id = "7b3e02b3-6b1a-4e75-bdad-cef5b279b074";
        final String companyName = "Cybersource";
        final String position = "Software Engineer - Mobile (m/f/d)";
        final String salary = "4k - 8k PLN";
        final String offerUrl = "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn";
        return sampleOfferDto(id, companyName, position, salary, offerUrl);
    }
}
