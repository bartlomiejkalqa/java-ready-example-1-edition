package com.junioroffers.offer.domain;

import java.time.Duration;

import com.junioroffers.JobOffersApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = OfferServiceRedisCachingWithContainerTest.TestConfig.class)
@ActiveProfiles("redistest")
@Testcontainers
class OfferServiceRedisCachingWithContainerTest {

    @MockBean
    OfferRepository offerRepository;

    @Container
    private static final GenericContainer<?> REDIS;

    private static final String REDIS_IMAGE_NAME = "redis";

    static {
        REDIS = new GenericContainer<>(REDIS_IMAGE_NAME)
                .withExposedPorts(6379);
        REDIS.start();
        System.setProperty("spring.redis.port", String.valueOf(REDIS.getFirstMappedPort()));
    }

    @Test
    public void should_invalidate_cache_automatically_after_given_time(@Autowired CacheManager cacheManager,
                                                                       @Autowired OfferService offerService) {
        final String expectedCacheName = "jobOffers";
        final int expectedTimesOfInvocation = 2;
        final Duration duration = Duration.ofSeconds(10);
        offerService.findAllOffers();
        assertThat(cacheManager.getCacheNames().contains(expectedCacheName)).isTrue();

        await()
                .atMost(duration)
                .untilAsserted(() -> {
                            offerService.findAllOffers();
                            verify(offerRepository, times(expectedTimesOfInvocation)).findAll();
                        }
                );
    }

    @Import(JobOffersApplication.class)
    static class TestConfig {

    }
}