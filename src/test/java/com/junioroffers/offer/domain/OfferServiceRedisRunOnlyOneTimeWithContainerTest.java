package com.junioroffers.offer.domain;

import com.junioroffers.JobOffersApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = OfferServiceRedisRunOnlyOneTimeWithContainerTest.TestConfig.class)
@ActiveProfiles("redistest")
@Testcontainers
class OfferServiceRedisRunOnlyOneTimeWithContainerTest {

    @MockBean
    OfferRepository offerRepository;

    private static final String REDIS_IMAGE_NAME = "redis";
    private static final int EXPOSED_PORT = 6379;

    @Container
    private static final GenericContainer<?> REDIS;

    static {
        REDIS = new GenericContainer<>(REDIS_IMAGE_NAME).withExposedPorts(EXPOSED_PORT);
        REDIS.start();
        System.setProperty("spring.redis.port", String.valueOf(REDIS.getFirstMappedPort()));
    }

    @Test
    public void should_create_cache_and_run_only_one_time(@Autowired CacheManager cacheManager,
                                                          @Autowired OfferService offerService) {
        final String expectedCacheName = "jobOffers";
        final int expectedTimesOfInvocation = 1;
        assertThat(cacheManager.getCacheNames().isEmpty()).isTrue();

        offerService.findAllOffers();
        offerService.findAllOffers();

        assertThat(cacheManager.getCacheNames().contains(expectedCacheName)).isTrue();
        verify(offerRepository, times(expectedTimesOfInvocation)).findAll();
    }

    @Import(JobOffersApplication.class)
    static class TestConfig {

    }
}