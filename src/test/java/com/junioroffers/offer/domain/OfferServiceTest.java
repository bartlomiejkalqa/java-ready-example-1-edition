package com.junioroffers.offer.domain;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.exceptions.OfferNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class OfferServiceTest implements SampleOfferDto, SampleOffer {

    OfferRepository offerRepository = Mockito.mock(OfferRepository.class);

    @Test
    public void should_return_two_expected_offers() {
        when(offerRepository.findAll()).thenReturn(Arrays.asList(
                sampleOffer("7b3e02b3-6b1a-4e75-bdad-cef5b279b074", "Cybersource", "Software Engineer - Mobile (m/f/d)", "4k - 8k PLN", "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn"),
                sampleOffer("24ee32b6-6b15-11eb-9439-0242ac130002", "CDQ Poland", "Junior DevOps Engineer", "8k - 14k PLN", "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd")));
        assertThat(offerRepository.findAll()).isEqualTo(Arrays.asList(
                sampleOffer("7b3e02b3-6b1a-4e75-bdad-cef5b279b074", "Cybersource", "Software Engineer - Mobile (m/f/d)", "4k - 8k PLN", "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn"),
                sampleOffer("24ee32b6-6b15-11eb-9439-0242ac130002", "CDQ Poland", "Junior DevOps Engineer", "8k - 14k PLN", "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd")));

        OfferService offerService = new OfferService(offerRepository);
        final List<OfferDto> expectedOffers = Arrays.asList(cybersourceOfferDto(), cdqPolandOfferDto());

        final List<OfferDto> allOffers = offerService.findAllOffers();

        then(allOffers).containsExactlyInAnyOrderElementsOf(expectedOffers);
    }

    @Test
    public void should_return_correct_offer_for_given_id_two() {
        when(offerRepository.findById("24ee32b6-6b15-11eb-9439-0242ac130002")).thenReturn(Optional.of(
                sampleOffer("24ee32b6-6b15-11eb-9439-0242ac130002", "CDQ Poland", "Junior DevOps Engineer", "8k - 14k PLN", "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd")));
        assertThat(offerRepository.findById("24ee32b6-6b15-11eb-9439-0242ac130002")).isEqualTo(Optional.of(
                sampleOffer("24ee32b6-6b15-11eb-9439-0242ac130002", "CDQ Poland", "Junior DevOps Engineer", "8k - 14k PLN", "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd")));

        OfferService offerService = new OfferService(offerRepository);

        final OfferDto offerById = offerService.findOfferById("24ee32b6-6b15-11eb-9439-0242ac130002");

        then(offerById).isEqualTo(cdqPolandOfferDto());
    }

    @Test
    public void should_return_correct_offer_for_given_id_one() {
        when(offerRepository.findById("7b3e02b3-6b1a-4e75-bdad-cef5b279b074")).thenReturn(Optional.of(
                sampleOffer("7b3e02b3-6b1a-4e75-bdad-cef5b279b074", "Cybersource", "Software Engineer - Mobile (m/f/d)", "4k - 8k PLN", "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn")));
        assertThat(offerRepository.findById("7b3e02b3-6b1a-4e75-bdad-cef5b279b074")).isEqualTo(Optional.of(
                sampleOffer("7b3e02b3-6b1a-4e75-bdad-cef5b279b074", "Cybersource", "Software Engineer - Mobile (m/f/d)", "4k - 8k PLN", "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn")));

        OfferService offerService = new OfferService(offerRepository);

        final OfferDto offerById = offerService.findOfferById("7b3e02b3-6b1a-4e75-bdad-cef5b279b074");

        then(offerById).isEqualTo(cybersourceOfferDto());
    }

    @Test
    public void should_throw_offer_not_found_exception_when_no_offer_with_given_id() {
        when(offerRepository.findById("100")).thenReturn(Optional.empty());
        assertThat(offerRepository.findById("100")).isEqualTo(Optional.empty());

        OfferService offerService = new OfferService(offerRepository);

        Throwable thrown = catchThrowable(() -> offerService.findOfferById("100"));

        assertThat(thrown)
                .isInstanceOf(OfferNotFoundException.class)
                .hasMessage("Offer with id 100 not found");
    }

    @Test
    public void should_correctly_parse_dto_and_add_to_repository() {
        final Offer offerToSave = sampleOfferWithoutId("test", "test", "test"
                , "testurl");
        final Offer offerAfterSave = sampleOffer("id2", "test", "test", "test"
                , "testurl");
        final OfferDto givenOfferDto = sampleOfferDtoWithoutId("test", "test", "test"
                , "testurl");
        final OfferDto expectedOfferDto = sampleOfferDto("id2", "test", "test", "test"
                , "testurl");
        when(offerRepository.save(offerToSave)).thenReturn(offerAfterSave);
        OfferService offerService = new OfferService(offerRepository);

        final OfferDto saveResult = offerService.saveOffer(givenOfferDto);

        then(saveResult).isEqualTo(expectedOfferDto);
        verify(offerRepository, times(1)).save(any());
    }

    @Test
    public void should_add_offer_with_autogenerated_id_when_already_set_in_offer_dto() {
        final Offer offerToSave = sampleOfferWithoutId("test", "test", "test"
                , "testurl");
        final Offer offerAfterSave = sampleOffer("id2", "test", "test", "test"
                , "testurl");
        final OfferDto givenOfferDto = sampleOfferDto("idToBeIgnored", "test", "test", "test"
                , "testurl");
        final OfferDto expectedOfferDto = sampleOfferDto("id2", "test", "test", "test"
                , "testurl");
        when(offerRepository.save(offerToSave)).thenReturn(offerAfterSave);
        OfferService offerService = new OfferService(offerRepository);

        final OfferDto saveResult = offerService.saveOffer(givenOfferDto);

        then(saveResult).isEqualTo(expectedOfferDto);
        verify(offerRepository, times(1)).save(any());
    }
}