package com.junioroffers.offer.domain;

import java.util.Optional;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.exceptions.OfferDuplicateException;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

@SpringBootTest(classes = OfferServiceSaveWithContainerTests.TestConfig.class)
@ActiveProfiles("container")
@Testcontainers
class OfferServiceSaveWithContainerTests implements SampleOffer, SampleOfferDto {

    private static final String MONGO_VERSION = "4.4.4";

    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo:" + MONGO_VERSION);

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    void should_add_offer_using_service(@Autowired OfferRepository repository,
                                        @Autowired OfferService service) {
        final OfferDto sampleOfferDto = sampleOfferDtoWithoutId("companynamefirstTest",
                "urlfirstTest",
                "positionfirstTest",
                "40kfirstTest");
        then(repository.findByOfferUrl("40kfirstTest")).isNotPresent();

        final OfferDto offerDto = service.saveOffer(sampleOfferDto);

        final Optional<Offer> offer = repository.findByOfferUrl("40kfirstTest");
        assertThat(offer).isPresent();
        final Offer actualOffer = offer.get();
        assertThat(actualOffer.getOfferUrl()).isEqualTo(offerDto.getOfferUrl());
        assertThat(actualOffer.getCompanyName()).isEqualTo(offerDto.getCompanyName());
        assertThat(actualOffer.getPosition()).isEqualTo(offerDto.getPosition());
        assertThat(actualOffer.getSalary()).isEqualTo(offerDto.getSalary());
    }

    @Test
    void should_not_add_offer_when_offer_with_url_already_exists_in_database(@Autowired OfferRepository repository,
                                                                             @Autowired OfferService service) {
        final OfferDto sampleOfferDto = sampleOfferDtoWithoutId("companynamefirstTest",
                "urlfirstTest",
                "positionfirstTest",
                "sameUrl");
        final Offer savedOffer = sampleOfferWithoutId("companynamefirstTest",
                "urlfirstTest",
                "positionfirstTest",
                "sameUrl");
        repository.save(savedOffer);
        then(repository.findByOfferUrl("sameUrl")).isPresent();

        Throwable thrown = catchThrowable(() -> service.saveOffer(sampleOfferDto));

        AssertionsForClassTypes.assertThat(thrown)
                .isInstanceOf(OfferDuplicateException.class)
                .hasMessage("Offer with offerUrl [sameUrl] already exists");
    }

    @Import(JobOffersApplication.class)
    static class TestConfig {

    }
}
