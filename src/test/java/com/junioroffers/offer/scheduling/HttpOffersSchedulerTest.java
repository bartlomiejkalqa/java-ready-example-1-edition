package com.junioroffers.offer.scheduling;

import java.time.Duration;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.infrastructure.RemoteOfferClient;
import org.junit.jupiter.api.Test;
import org.mockito.verification.VerificationMode;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = HttpOffersSchedulerTest.TestConfig.class)
@ActiveProfiles("schedulertest")
class HttpOffersSchedulerTest {

    @SpyBean
    RemoteOfferClient offerHttpClient;

    @Test
    public void should_run_http_client_offers_fetching_exactly_given_times() {
        final VerificationMode givenTimes = times(2);
        final Duration givenWaitingTime = Duration.ofSeconds(10);

        await()
                .atMost(givenWaitingTime)
                .untilAsserted(() -> verify(offerHttpClient, givenTimes).getOffers());
    }

    @Import(JobOffersApplication.class)
    static class TestConfig {

    }
}