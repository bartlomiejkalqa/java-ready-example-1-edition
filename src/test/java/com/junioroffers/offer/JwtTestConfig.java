package com.junioroffers.offer;

import java.util.Collections;

import com.junioroffers.security.jwt.JwtAuthTokenFilter;
import com.junioroffers.security.jwt.JwtParser;
import com.junioroffers.security.jwt.JwtUtils;
import com.junioroffers.security.login.SecurityContextUpdater;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.AuthenticationEntryPoint;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestConfiguration
class JwtTestConfig {

    @Bean
    AuthenticationEntryPoint authenticationEntryPoint() {
        return mock(AuthenticationEntryPoint.class);
    }

    @Bean
    JwtUtils jwtUtils() {
        final JwtUtils mock = mock(JwtUtils.class);
        when(mock.validateJwtToken(any())).thenReturn(true);
        when(mock.getUserNameFromJwtToken(any())).thenReturn("user");
        return mock;
    }

    @Bean
    JwtParser jwtParser() {
        final JwtParser mock = mock(JwtParser.class);
        when(mock.parse(any())).thenReturn("jwt");
        return mock;
    }

    @Bean
    SecurityContextUpdater securityContextUpdater() {
        return new SecurityContextUpdater();
    }

    @Bean
    JwtAuthTokenFilter authTokenFilter() {
        return new JwtAuthTokenFilter(userDetailsService(), jwtUtils(), jwtParser(), securityContextUpdater());
    }

    @Bean
    UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
                return new User("user", "password", Collections.emptyList());
            }
        };
    }
}
